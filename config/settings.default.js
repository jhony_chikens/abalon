exports.getSettings = function () {
    return {
        mongoDbUri: 'mongodb://<user>:<password>@<url>:<port>/<db>',
        appPort: 3000,
        socketsMainPort: 3001,
        socketsGamePort: 3003
    };
};