exports.constant = function () {
  return {
    GAME_STATUS_WAIT : 0,
    GAME_STATUS_ACTIVE : 1,
    GAME_STATUS_OLD : 2,

    PLAYER_STATUS_PASSIVE: 0,
    PLAYER_STATUS_READY: 1
  };
};