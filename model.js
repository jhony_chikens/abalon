var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Player = new Schema({
  name: {type: String, required: true},
  status: {type: Number, default: 0}
});

var Game = new Schema({
  gameId  : ObjectId,
  players : {
    first  : [Player],
    second : [Player]
  },
  date    : { type: Date, default: Date.now },
  status  : Number
});

mongoose.model('Game', Game);
mongoose.model('Player', Player);

exports.Game = function(db) {
  return db.model('Game');
};

exports.Player = function(db) {
  return db.model('Player');
};