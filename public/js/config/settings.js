var Settings = {
    maxRow: 9,
    equatorRow: 5,
    equatorLine: 9,
    minRowLenght: 5,
    firstTurn: 'blue',
    pointsToWin: 6,
    players: {
    	'red': {
    		cells: [
                {x: 1, y: 5}, 
                {x: 1, y: 7},
                {x: 1, y: 9}, 
                {x: 1, y: 11},
                {x: 1, y: 13},
                
                {x: 2, y: 4},
                {x: 2, y: 6},    
                {x: 2, y: 8},
                {x: 2, y: 10},
                {x: 2, y: 12},
                {x: 2, y: 14},
                
                {x: 3, y: 7},    
                {x: 3, y: 9},
                {x: 3, y: 11},


            ]
    	}, 
    	'blue': {
    		cells: [
                {x: 9, y: 5}, 
                {x: 9, y: 7},
                {x: 9, y: 9}, 
                {x: 9, y: 11},
                {x: 9, y: 13},
                
                {x: 8, y: 4},
                {x: 8, y: 6},    
                {x: 8, y: 8},
                {x: 8, y: 10},
                {x: 8, y: 12},
                {x: 8, y: 14},
                
                {x: 7, y: 7},    
                {x: 7, y: 9},
                {x: 7, y: 11}
    	   ]
        }
	}
}
