function CellObject($cell)
{
	this.x = $cell.data('x');
	this.y = $cell.data('y');

	this.active = false;
	this.player = false;

	this.cell = $cell
}

CellObject.prototype.setActive = function() {
	if (!this.active) {
		this.active = true;
		return true; 
	}
	return false;
}

CellObject.prototype.setPlayer = function(player) {
	if (this.setActive()) {
		if (player == 'blue') {
			this.setBlue();
		}
		else {
			this.setRed();
		}
	}
}

CellObject.prototype.setBlue = function() {
		this.player = 'blue';
		//this.cell.addClass('active-blue')	
		this.cell.attr({fill: "#b1c9ed"})
}

CellObject.prototype.setRed = function() {
		this.player = 'red';
		this.cell.attr({fill: "#edb1b1"})	
}

CellObject.prototype.setPassive = function() {
	if (this.active) {
		this.active = false;
		this.player = false;
		this.cell.attr({fill: '#fff'});
	}
}

CellObject.prototype.setTaken = function() {
	if (this.active) {
		this.cell.attr({'stroke-width': 2, 'stroke': '#a4ef83'});
	}
}

CellObject.prototype.setFree = function() {
	this.cell.attr({'stroke-width': 1, 'stroke': '#fff'});
}

CellObject.prototype.getRight = function() {
	return {
		'x': parseInt(this.x),
		'y': parseInt(this.y) + 2
	}
}

CellObject.prototype.getNextByDirection = function(direction) {
	direction = helper.capitaliseFirst(direction);
	return this['get' + direction]();
}


CellObject.prototype.getLeft = function() {
	return {
		'x': parseInt(this.x),
		'y': parseInt(this.y) - 2
	}
}

CellObject.prototype.getUpLeft = function() {
	return {
		'x': parseInt(this.x) - 1,
		'y': parseInt(this.y) - 1
	}
}

CellObject.prototype.getUpRight = function() {
	return {
		'x': parseInt(this.x) - 1,
		'y': parseInt(this.y) + 1
	}
}

CellObject.prototype.getDownLeft = function() {
	return {
		'x': parseInt(this.x) + 1,
		'y': parseInt(this.y) - 1
	}
}

CellObject.prototype.getDownRight = function() {
	return {
		'x': parseInt(this.x) + 1,
		'y': parseInt(this.y) + 1
	}
}

CellObject.prototype.doMirror = function() {
	var equatorRow = Settings.equatorRow,
		equatorLine = Settings.equatorLine;

		return {
			x: equatorRow + (equatorRow - parseInt(this.x)),
			y: equatorLine + (equatorLine - parseInt(this.y))
		}
};
