var Game = {

    map: false,

    controller:false,    

    onFieldClick: function(event) {
        var x = this.data('x'),
            y = this.data('y'),
            callbacks = $.Callbacks('stopOnFalse'),

            args,
            player = Players.getPlayer(), 
            map = Game.map,
            cell = map.getCell(x, y);

        if (player.getTakenCells().length == 1 && cell.player == false ) {
            callbacks
                .add(map.goTo)
                .add(player.popCell)
                .add(Game.checkIsWin)
                .add(Players.changePlayer)
                .add(Game.hideArrows)
                .add(Game.sendTurn);
            args = {
                from: player.getTakenCells()[0], 
                to: {x: x, y: y}
            }
        }
        else {
            var validator = new TakenValidator(map);
            callbacks
                .add(validator.check)
                .add(map.checkCellInList)
                .add(map.takeCell)
                .add(player.pushCell)
                .add(Game.showArrowsIfNeed);
            args = {x: x, y: y} 
        }

        args.player = player    
        callbacks.fire(args)
    },

    onDirectionButtonClick: function(event) {
        var callbacks = $.Callbacks('stopOnFalse'),
            map = event.data.map,
            validator = new MovingValidator(map);

        callbacks
            .add(validator.check)
            .add(map.multiCellTurn)
            .add(Game.sendMultiTurn)
            .add(Players.cleanTakenCells)
            .add(Game.checkIsWin)
            .add(Players.changePlayer)
            .add(Game.hideArrows);

        var args = {
            direction: $(this).data('direction'),
            player: Players.getPlayer()
        };

        callbacks.fire(args);
    },

    showArrowsIfNeed: function(params) {
        if (params.player.getTakenCells().length > 1) {
            Game.showArrows();
        }
    },

    showArrows: function() {
        $('.turn-direction-wrap').fadeIn('fast');
    },

    hideArrows: function() {
        $('.turn-direction-wrap').fadeOut('fast');
    },

    generateField: function (minRowLenght, maxRow, equatorRow) {
        var result = [],
            rowLength = indent = minRowLenght;

        for (var i = 0; i < maxRow; i++) {
            var cells = [];
            for (var j = 0; j < rowLength; j++) {
                var x = i+1,
                    y = (j*2) + indent;
                cells.push({x: x, y: y});
            };

            result.push({
                cells: cells,
                indent: indent,
            });

            if (i < equatorRow - 1) {
                indent--;
                rowLength++;
            }
            else {
                indent++;
                rowLength--;
            }
        }

        return result;
    },

    initCanvas: function () {
        var paper = new Raphael(document.getElementById('canvas-container'), 550, 373),
            testField;

        Settings['field'] = Game.generateField(Settings.minRowLenght, Settings.maxRow, Settings.equatorRow);
        
        function setCircle (x, y, indent) {
            var canvasY = ((x - 1) * 40) + 30,
                xDelta = (indent - 1) * 25; 

            var canvasX =((y + 1)  * 50) + xDelta;

           return paper.circle(canvasX, canvasY, 20)
                .attr({fill: "#fff", stroke: '#fff', cursor: 'pointer'})
        }

        _.each(Settings['field'], function (item) {
            _.each(item.cells, function (cell, i) {
               var circle = setCircle(cell.x, i, item.indent);
               circle.data('x', cell.x).data('y', cell.y)
            })
        });

        return paper;
    },
    
    sendTurn: function (params) {
        params.playerLose = params.player.getIsWin();
        Game.controller.sendTurn('turn', params);
    },
    
    sendMultiTurn: function (params) {
        var cells = params.player.getTakenCells();
        Game.controller.sendTurn('multi_turn', {
            cells : cells,
            direction: params.direction,
            playerLose: params.player.getIsWin()
        });
    },

    turnCallback: function (data) {
        var player = Players.getPlayer(), 
            callbacks = $.Callbacks('stopOnFalse'),
            fromCell = Game.map.getCell(data.from.x, data.from.y),
            toCell = Game.map.getCell(data.to.x, data.to.y);

        var from = fromCell.doMirror();
        var to = toCell.doMirror();

        callbacks
            .add(Game.map.goTo)
            .add(Players.changePlayer);
        callbacks.fire({
            from: from,
            to: to,
            player: Players.getPlayer()
        })
    },

    multiTurnCallback: function (data) {
        console.log(data);
        var player = Players.getPlayer(), 
            callbacks = $.Callbacks('stopOnFalse'),
            map = Game.map,
            cells = data.cells,
            direction = helper.getOppositeDirection(data.direction);

        _.each(cells, function (item) {
            var cell = map.getCell(item.x, item.y);
            player.pushCell(cell.doMirror());
        });

        callbacks
            .add(map.multiCellTurn)
            .add(Players.cleanTakenCells)
            .add(Players.changePlayer);
        callbacks.fire({
            direction: direction,
            player: player
        });
    },

    checkIsWin: function (params) {
        if (params.player.getIsWin()) {
            Game.controller.setPlayerWin();
        }
    }

}