function GameController (socket) {
    this.socket = socket;
    this.turnCallback = false;
    this.multiTurnCallback = false; 
    this.gameId = $('#game-field').data('game_id');
    this.playerTitle = $('#currentPlayer').data('player');
    this.statusLine = $('div.status-line .status');
    this.loader = $('#game-loader');

    this.gameOverImg = $('#game-over');

    var self = this;

    this.socket.emit('game_start', {gameId: this.gameId});

    this.socket.on('opponent_turn', function (data) {
        self.onTurn(data, self.turnCallback);
    });

    this.socket.on('opponent_multi_turn', function (data) {
        self.onTurn(data, self.multiTurnCallback);
    });

    this.socket.on('opponent_leave', function (data) {
        self.onOpponentLeave();
    });
}

GameController.prototype.setTurnCallback = function(callback) {
    this.turnCallback = callback;
    return this;
};

GameController.prototype.setMultiTurnCallback = function(callback) {
    this.multiTurnCallback = callback;
    return this;
};

GameController.prototype.showLoader = function() {
    this.loader.show();
};

GameController.prototype.hideLoader = function() {
    this.loader.hide();
};

GameController.prototype.startTimer = function() {
    var text = 'Game will start after: ',
        times = 5;
    var callback = this.startGame;    
    var self = this;
    this.showLoader();
    var interval = setInterval(function () {
        if (times === 0) {
            clearInterval(interval);
            callback.call(self);
            return;
        }
        self.statusLine.text(text + times--);
    }, 1000)

};

GameController.prototype.startGame = function() {
    if (this.playerTitle == 'second') {
        this.statusLine.text('Waiting for opponent turn');
    } 
    else {
        this.statusLine.text('Game started. Now your turn!');
        this.hideLoader();
    }
};

GameController.prototype.sendTurn = function(trunName, args) {
    this.statusLine.text('Waiting for opponent turn')
    this.showLoader();
    this.socket.emit(trunName, {
        turnData: args,
        gameId: this.gameId
    });
};

GameController.prototype.onTurn = function(data, callback) {
    if (!data.turnData.playerLose) {
        callback(data.turnData);
        this.hideLoader();
        this.statusLine.text('Now your turn!');
    }
    else {
        this.hideLoader();
        this.statusLine.text('Sorry but you lose :*( try again!');
        this.gameOverImg.show();
    }
};

GameController.prototype.onOpponentLeave = function() {
    this.statusLine
        .text('Sorry, but your opponent leave the game. Game is closed :(');
    this.gameOverImg.show();
};

GameController.prototype.setPlayerWin = function() {
   this.statusLine.text('Congrats!!! You are win!');
   this.gameOverImg.show();
};