function Map(canvas, settings) 
{
    var cellMap = {};
    //init field

    canvas.forEach(function(el) {
        var id = el.data('x') + '_' + el.data('y')
        cellMap[id] = new CellObject(el)       
    })

    
    //return true if cellA is near by cellB 
    //in other cases return false
    function isNear(cellA, cellB) {
        if (Math.abs(cellA.x - cellB.x) >= 2 || 
            Math.abs(cellA.y - cellB.y) >= 3){
            return false;
        }
        else {
            return true;
        }
    }


    //return true if cell is near by even one cell in list
    //in other cases return false
    function nearByGroup(list, cell) {
        return _.any(list, 
            function(obj){ 
                return isNear(obj, cell); 
            });
    }

    //analise two numbers in the line and 
    //get third based on their differences
    function getThirdInLine(first, second) {
        var delta = Math.abs(second - first),
            first = parseInt(first),
            second = parseInt(second),
            third;

        if (second == first) {
            return second
        }
        else {
            return (first < second) 
                ? second + delta : second - delta   
        }

    }

    //get coords of next cell 
    // from - first cell 
    // to - second cell
    function getNext(from, to) {
        return {
            x: getThirdInLine(from.x, to.x),
            y: getThirdInLine(from.y, to.y)
        };
    }

    function getCell(x, y) {
        var cellId = x + '_' + y;
        return (cellMap[cellId] == undefined) ? false : cellMap[cellId];
    }

    function goToCell(from, to, playerColor) {
        var fromCell = getCell(from.x, from.y),
            toCell = getCell(to.x, to.y)

        if (isNear(from, to)) {
            if (toCell.active) {
                goToCell(to, getNext(from, to), toCell.player);
            }

            fromCell.setFree();
            fromCell.setPassive();
            if (toCell) {
                toCell.setPlayer(playerColor);
            }
            else {
                Players.upScore()
            }
            return true;
        }
        alert('You cant go here!!');
        return false;
    }

    function isCellInList(cellList, cell){
        return _.any(cellList, function(obj){
            return (obj.x == cell.x && obj.y == cell.y) 
        })
    }

    function sortByDirection(cellList, direction) {
        
        var getCellWeight = function(cell) {
            var nextCell = getCell(cell.x, cell.y)
                .getNextByDirection(direction)
        
            if (isCellInList(cellList, nextCell)) {
                return 1 + getCellWeight(nextCell, direction)
            }
            else {
                return 1
            }
        }

        return _.sortBy(cellList, getCellWeight)
    }


    return {
        getCellMap: function() {
            return cellMap;
        },

        getCell: function(x, y) {
            return getCell(x, y)
        },

        nearByGroup: function (cellGroup, cell) {
            return nearByGroup(cellGroup, cell);
        },

        takeCell: function(params) {
            var cell = getCell(params.x, params.y);
            
            cell.setTaken();
            return true;
        },

        goTo: function(params) {
            return goToCell(params.from, params.to, params.player.getColor());
        },

        multiCellTurn: function(params) {
            var takenCells = params.player.getTakenCells();
               
            takenCells = sortByDirection(takenCells, params.direction);

            _.each(takenCells, function(item) {
                var cell = getCell(item.x, item.y);
                var nextCell = cell.getNextByDirection(params.direction);

                goToCell(cell, nextCell, params.player.getColor())
            });
            return true;
        },

        checkCellInList: function (params) {
            var takenCells = params.player.getTakenCells(),
                cell = getCell(params.x, params.y);

            if (isCellInList(takenCells, cell)) {
                params.player.removeCell(cell);
                cell.setFree();
                return false;
            }

            return true;
        },
        
        isCellInList: function (list, cell) {
            return isCellInList(list, cell);
        },

        isCellInLine: function (line, cell) {
            if (line.length < 2){
                return true;    
            }
            nextCell = getNext(line[0], line[1])
            if (nextCell.x == cell.x && nextCell.y == cell.y) {
                return true
            }
            nextCell = getNext(line[1], line[0])
            if (nextCell.x == cell.x && nextCell.y == cell.y) {
                return true
            }
            return false
        }        
    }
}