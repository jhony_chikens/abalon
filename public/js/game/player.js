var Players = {
    
    players:{},

    pointsToWin: 0,

    currentPlayer: 'blue',

    changePlayer: function(params) {
        if (params.player.getColor() == 'blue') {
            Players.currentPlayer = 'red';
            $('.red .turn').show();
            $('.blue .turn').hide();
        }
        else {
            Players.currentPlayer = 'blue';
            $('.blue .turn').show();
            $('.red .turn').hide(); 
        }
        return true;
    },

    getPlayer: function () {
        return Players.players[Players.currentPlayer];
    },

    init: function (settings, map) {
        var currentPlayerTitle = $('#currentPlayer').data('player');

        Players.currentPlayer = (currentPlayerTitle == 'second') ? 'red' : 'blue';
        _.each(settings.players, function(player, color){
            Players.players[color] = new Player(color, player.cells, map);
        });
    },

    cleanTakenCells: function() {
        Players.getPlayer().cleanTakenCells();
        return true;
    },

    upScore: function () {
        var player = Players.getPlayer();
        player.upScore();
        
        $('.' + player.getColor() + '-score').text(player.getScore());

        if (Settings.pointsToWin <= player.getScore()) {
            player.setIsWin();
        }

        return true;
    }
};


function Player (color, cells, map) {
    
    var takenCells = [];

    var score = 0;

    var isWin = false;

    _.each(cells, function(cell) {
        var cellObject = map.getCell(cell.x, cell.y);
        try {
            cellObject.setPlayer(color);
        } catch(e) {
            throw new Error('Cant create cell x: ' + cell.x + ' y:' + cell.y );    
        }
    });

    return {
        getTakenCells: function() {
            return takenCells;
        },

        upScore: function() {
            score += 1;
        },
        getScore: function() {
            return score;
        },

        getColor: function() {
            return color;
        },

        popCell : function () {
            return takenCells.pop();
        },

        pushCell: function (cell) {
            takenCells.push({x: cell.x, y: cell.y});
            return true;
        },

        cleanTakenCells: function() {
            takenCells = [];
        },

        removeCell: function(cell) {
            var removelKey;
            _.each(takenCells, function(item, key) {
                if (item.x == cell.x && item.y == cell.y) {
                    removelKey = key;
                }
            });
            
            takenCells.splice(removelKey, 1);
        },

        setIsWin: function () {
            isWin = true;
        },

        getIsWin: function () {
            return isWin;
        }
    };
}