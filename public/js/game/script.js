/* Author:

*/
$(document).ready(function(){
    var socket = io.connect(window.socketSettings.gameUrl);
    var gameController = new GameController(socket);
    
    gameController.setTurnCallback(Game.turnCallback);
    gameController.setMultiTurnCallback(Game.multiTurnCallback);
    gameController.startTimer();
     
    var canvas = Game.initCanvas();

    var map = new Map(canvas, Settings);
    Players.init(Settings, map)

    Game.map = map;
    Game.controller = gameController;

    canvas.forEach(function(el) {
    	el.click(Game.onFieldClick)
    })

    $('div.turn-direction-wrap input').on('click', 
        {map: map}, 
        Game.onDirectionButtonClick
    );
});