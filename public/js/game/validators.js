function MovingValidator (map) {
    var player,
        direction;

    

    function setParams (params) {
        player = params.player;
        direction = params.direction;
    }

    function getBlockWeight (cellObject, direction) {
        var nextInBLock = cellObject.getNextByDirection(direction),
            nextCellObj = map.getCell(nextInBLock.x, nextInBLock.y);

        if (nextCellObj && nextCellObj.player == cellObject.player) {
            return 1 + getBlockWeight(nextCellObj, direction);
        }
        else {
            return 1;
        }
    }

    function checkBlock (cellA, cellB, direction) {
        var oppositeDirection = helper.getOppositeDirection(direction),
            blockA = getBlockWeight(cellA, direction),
            blockB = getBlockWeight(cellB, oppositeDirection);

        return (blockA < blockB) ? true : false;
    }
    
    return {
        check: function (params) {
            var result = true;
            setParams(params);
            try {
                _.each(player.getTakenCells(), function (cell) {
                    var cellObj = map.getCell(cell.x, cell.y),
                        nextCell= cellObj.getNextByDirection(direction);

                    nextCellObj = map.getCell(nextCell.x, nextCell.y);
                    if (!nextCellObj){
                        throw new Error('You cannt go there');
                    }
                    if (nextCellObj.player == player.getColor() &&
                        !map.isCellInList(player.getTakenCells(), nextCellObj) &&
                        player.getTakenCells().length > 2 ) {
                        
                        throw new Error('You cannt push this cell');
                    }

                    if (nextCellObj.player !== player.getColor() &&
                        nextCellObj.player !== false) {
                        
                        if (!checkBlock(nextCellObj, cellObj, direction)) {
                            throw new Error('You cannt push this cell');
                        }
                    }
                });

                return true;
            } catch(e) {
                alert(e.message);
                return false;    
            }
        }
    };
}

function TakenValidator (map) {
    return {
        check: function (params) {
            var cell = map.getCell(params.x, params.y),
                takenCells = params.player.getTakenCells();
            try {
                if (map.isCellInList(takenCells, cell)){
                    return true;
                }

                if ((takenCells.length + 1) > 3) {
                    throw new Error('You cant take more than 3 cell'); 
                }

                if (!cell.player) {
                    throw new Error('You cant take passive cell');
                }

                if (cell.player !== params.player.getColor()) {
                    throw new Error('You cant take cell of opposite player');
                }

                if (takenCells.length !== 0 && !map.nearByGroup(takenCells, cell)){
                    throw new Error('You cant take this cell');
                }

                if (!map.isCellInLine(takenCells, cell)) {
                   throw new Error('You should take inline cells'); 
                }
            } catch(e) {
                alert(e.message);
                return false;
            }

            return true;
        }
    };
}