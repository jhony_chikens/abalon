(function($) {
    "use strict";
    $(document).ready(function() {
        var socket = io.connect(window.socketSettings.mainUrl);
        
        socket.on('game_created', Main.addNewGame);
        socket.on('game_removed', Main.removeGame);
        socket.on('handshake_request', Main.handshakeHandler);
        socket.on('handshake_result', function (data) {
             Main.handshakeGetResult(data, socket);
        });
        socket.on('game_started', Main.gameStarted);
        $('#join-game-modal .btn-primary')
            .on('click', {socket: socket}, Main.joinGame);

        $('#start-game-modal .btn-primary')
            .on('click', {socket: socket}, Main.createGame);

        $('#confirm-modal').on('click', '.btn',
            {socket: socket}, Main.handshakeSendResult);

        $('#start-game').on('click',
            function () {
                $('#start-game-modal').modal();
            }
        );

        $('table').on('click', '.join-game-button',
            Main.showJoinGameWindow
        );


    });

    var Main = {
        createGame : function (event) {
            var socket = event.data.socket;
            var playerName = $('#start-game-modal .player-name').val();
            if (playerName === '') {
                alert('You should set your name');
                return false;
            }
            $('#start-game-modal').modal('hide');
            socket.emit('create_game', {playerName: playerName});
        },

        addNewGame : function (data) {
            var game = data.game;
            if (game._id) {
                var lineId = 'line_' + game._id;
                var playerFirst = game.players.first.pop();
                var html =
                    "<tr id='" + lineId + "' style='display:none;'><td>" + playerFirst.name + "</td>";
                if (data.currentPlayer) {
                    html += "<td class='content'><span class='badge badge-success'>Waiting for opponent</span></td>";
                }
                else{
                    html += "<td class='content'><input class='btn btn-primary join-game-button' type='button' data-id='" + game._id + "' value='join game'/></td>";
                }
                html += "</tr>";
                $('.table-condensed').append(html);
                $('#'+lineId).fadeIn();
            }
        },

        removeGame : function (data) {
            console.log('removed game', data);
            if (data._id) {
                var lineId = 'line_' + data._id;
                $('#' + lineId).fadeOut();
            }
        },

        gameStarted : function (data) {
            var lineId = 'line_' + data.gameId;
            var html = '<span class="badge">vs</span> ' + data.secondPlayer;
            $('#' + lineId).find('.content').html(html); 
        },

        showJoinGameWindow : function () {
            var gameId = $(this).data('id');
                $('#join-game-modal').modal();
                $('#join-game-modal')
                    .find('.btn-primary')
                    .data('id', gameId);
        },

        joinGame : function (event) {
            var socket = event.data.socket,
                gameId = $(this).data('id'),
                playerName = $('#join-game-modal .player-name').val();
            
            gameId = gameId.replace(/"/gi,'');
            if (playerName === '') {
                alert('You should set your name');
                return false;
            }

            $('#join-game-modal').modal('hide');
            socket.emit('handshake_start',
                {playerName: playerName, gameId: gameId});
            var $tableLine = $('#line_' + gameId);
            $tableLine.find('.content')
                .html('<span class="badge">sending request</span>');

        },

        handshakeHandler: function (data) {
            $('#confirm-modal').modal();
            $('#confirm-modal').find('.player-name')
                .text(data.playerName);
            $('#confirm-modal').find('.btn').each(function (i, item) {
                $(item).data('id', data.gameId);
            });
                
        },

        handshakeSendResult: function (event) {
            var gameId = $(this).data('id'),
                result = $(this).data('result'),
                playerName = $('#confirm-modal').find('.player-name').text(),
                socket = event.data.socket;

            socket.emit('handshake_response', {
                    gameId: gameId, 
                    result: result,
                    playerName: playerName
            });
        },

        handshakeGetResult: function (data, socket) {
            if (data.result) {
                window.location = '/game?id=' + data.gameId +'&player=' + data.player;
            }
            else {
                socket.emit('leave_room', {gameId: data.gameId});
                var $tableLine = $('#line_' + data.gameId);
                
                $tableLine.find('.content span.badge')
                    .addClass('badge-warning')
                    .text('sorry this player didnt want to play with you');
            }
        }
    };
})(jQuery);