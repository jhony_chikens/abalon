(function ($) {
    "use strict";

    $(document).ready(function () {
        $('a.rules-link').on('click', function (event) {
            $('#rules').modal();
            return false;
        });
    });
})
(jQuery);