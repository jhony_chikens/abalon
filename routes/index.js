/*
 * GET home page.
 */
var settings = require('../config/settings.js').getSettings(),
    cons = require('../const.js').constant();

var sockets = require('../sockets.js'),
    mongoose = require('mongoose');

var db  = mongoose.createConnection(settings.mongoDbUri),
    Game = require('../model.js').Game(db);


exports.index = function(req, res){

  sockets.main(settings, Game);

  Game.find({status: cons.GAME_STATUS_WAIT}, function (err, docs) {
    if (err) {
      console.log(err);
    }
    var documents = docs.map(function (doc) {
      var firstPlayer = doc.players.first.pop();
      return {name: firstPlayer.name, id: doc._id};
    });
    res.render('index', {title: 'Abalon', documents: documents});
  });
};

exports.deleteAll = function(req, res){
  Game.remove({}, function (err) {
    if (err) {
      console.log(err);
    }
    res.render('delete', {title: 'Abalon', result: 'Success'});
  });
};


exports.game = function (req, res) {

  var gameId = req.query.id,
      playerTitle = req.query.player;
  var opponentTitle = (playerTitle === 'second') ? 'first' : 'second';
  sockets.game(settings);

  Game.findById(gameId, function (err, game) {

    var currentPlayer = game.players[playerTitle].pop(),
        opponentPlayer = game.players[opponentTitle].pop();

    res.render('game',
      {
        title: 'Abalon',
        gameId: gameId,
        currentPlayer: playerTitle,
        opponentPlayer: opponentTitle,
        currentPlayerName: currentPlayer.name,
        opponentPlayerName: opponentPlayer.name
      }
    );
  });

};


exports.categories = function (req, res) {
  res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify({'s': 1}));
};


