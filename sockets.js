var async = require('async');
var cons = require('./const.js').constant();

exports.main = function (settings, Game) {
  "use strict";

  var io = require('socket.io').listen(settings.socketsMainPort);

  io.sockets.on('connection', function (socket) {

    socket.on('create_game', function (data) {
      var game = new Game();
      game.players.first.push({name: data.playerName});
      game.status = cons.GAME_STATUS_WAIT;
      game.save(function (err) {
        if (err) {
          return console.log(err);
        }

        socket.join('game_' + game._id);
        socket.set('myGameId', game._id);
        socket.emit('game_created',
          {game: game, currentPlayer: true});
        socket.broadcast.emit('game_created',
          {game: game, currentPlayer: false});

      });
    });


    socket.on('handshake_start', function (data) {
      var roomName = 'game_' + data.gameId;
      socket.join(roomName);
      socket.broadcast.to(roomName).emit('handshake_request',
          {playerName: data.playerName, gameId: data.gameId});
    });


    socket.on('handshake_response', function (data) {
      var roomName = 'game_' + data.gameId,
          result = {gameId: data.gameId};

      if (data.result) {
        Game.findById(data.gameId, function (err, game) {
          game.players.second.push({name: data.playerName});
          game.status = cons.GAME_STATUS_ACTIVE;
          game.save(function (err) {
            if (err) {
              return console.log(err);
            }

            result.result = true;
            result.player = 'first';
            socket.emit('handshake_result', result);

            result.player = 'second';
            socket.broadcast.to(roomName).emit('handshake_result',result);

            socket.broadcast.emit('game_started', {
              gameId: data.gameId,
              secondPlayer: data.playerName
            });
          });
        });
      } else {
        result.result = false;
        result.player = 'second';
        socket.broadcast.to(roomName).emit('handshake_result', result);
      }
    });

    socket.on('leave_room', function (data) {
      var roomName = 'game_' + data.gameId;
      socket.leave(roomName);
    });

    socket.on('disconnect', function () {
      socket.get('myGameId', function (err, id) {
        if (id) {
          Game.findById(id, function (err, game){
            if (err) {
              return console.log(err);
            }

            if (!game.players.second.length) {
              socket.emit('game_removed', game);
              socket.broadcast.emit('game_removed', game);
              game.remove();
            }
          });
        }
      });
    });
  });
};


exports.game = function (settings) {
  "use strict";
  var io = require('socket.io').listen(settings.socketsGamePort);

  io.sockets.on('connection', function (socket){

    socket.on('game_start', function (data) {
      socket.join('game_' + data.gameId);
      socket.set('myGameId', data.gameId);
    });

    socket.on('turn', function (data) {
      var roomName = 'game_' + data.gameId;
      socket.broadcast.to(roomName)
        .emit('opponent_turn', {turnData: data.turnData});
    });

    socket.on('multi_turn', function (data) {
      var roomName = 'game_' + data.gameId;
      socket.broadcast.to(roomName)
        .emit('opponent_multi_turn', {turnData: data.turnData});
    });

    socket.on('disconnect', function () {
      socket.get('myGameId', function (err, id) {
        var roomName = 'game_' + id;
        socket.broadcast.to(roomName)
          .emit('opponent_leave', {});
      });
    });
  });
};


